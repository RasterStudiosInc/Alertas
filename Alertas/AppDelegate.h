//
//  AppDelegate.h
//  Alertas
//
//  Created by Alan Villeda Ramirez on 17/06/17.
//  Copyright © 2017 Alan Villeda Ramirez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

