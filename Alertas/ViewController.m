//
//  ViewController.m
//  Alertas
//
//  Created by Alan Villeda Ramirez on 17/06/17.
//  Copyright © 2017 Alan Villeda Ramirez. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)enviarMensaje:(UIButton *)sender {
    
//    UIAlertController *alerta = [UIAlertController   alertControllerWithTitle:@"Mi primera app" message:@"Este es el mesaje de mi App" preferredStyle: UIAlertControllerStyleAlert];
//    
//    UIAlertAction *aceptar = [UIAlertAction actionWithTitle:@"Aceptar" style: UIAlertActionStyleDefault handler: nil];
//    
//    [alerta addAction:aceptar];
//    
//    [self presentViewController:alerta animated:true completion:nil];
    
    NSString *nombre = self.ViewTextField.text;
    NSString *nombreCompleto =  [NSString stringWithFormat:@"Hola, %@!", nombre];
    [self.miEtiqueta setText:nombreCompleto];
    
}


@end
